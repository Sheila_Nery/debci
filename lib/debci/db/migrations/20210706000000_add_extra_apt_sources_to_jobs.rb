class AddExtraAptSourcesToJobs < Debci::DB::LEGACY_MIGRATION
  def up
    add_column :jobs, :extra_apt_sources, :text
  end

  def down
    remove_column :jobs, :extra_apt_sources
  end
end
